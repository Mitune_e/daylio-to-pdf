export interface Goal {
    goal_id: number;
    id_challenge: number;
    order: number;
    reminder_enabled: boolean;
    reminder_minute: number;
    repeat_type: number;
    id_icon: number;
    repeat_value: number;
    end_date: number;
    id_avatar: number;
    created_at: number;
    id_tag: number;
    reminder_hour: number;
    state: number;
}

export interface TagGroup {
    id: number;
    name: string;
    is_expanded: boolean;
    order: number;
}

export interface GoalSuccessWeek {
    create_at_year: number;
    create_at_month: number;
    year: number;
    week: number;
    goal_id: number;
    create_at_day: number;
}

export interface Pref {
    pref_name: string;
    value: number;
    key: string;
}

export interface Reminder {
    state: number;
    custom_text_enabled: boolean;
    id: number;
    hour: number;
    minute: number;
}

export interface Asset {
    id: number;
    createdAt: number;
    createdAtOffset: number;
    type: number;
    checksum: string;
}

export interface GoalEntry {
    id: number;
    hour: number;
    minute: number;
    createdAt: number;
    goalId: number;
    month: number;
    second: number;
    day: number;
    year: number;
}

export interface Milestone {
    year: number;
    month: number;
    isAnniversary: boolean;
    note: string;
    name: string;
    categoryId: number;
    createdAt: number;
    createdAtOffset: number;
    day: number;
    hasCustomPhoto: boolean;
    assetId: number;
    remindersValue: string;
    id: number;
    predefinedMilestoneId: number;
    stateValue: number;
}

export interface CustomMood {
    state: number;
    mood_group_id: number;
    predefined_name_id: number;
    custom_name: string;
    icon_id: number;
    mood_group_order: number;
    id: number;
}

export interface Tag {
    name: string;
    id: number;
    createdAt: number;
    id_tag_group: number;
    order: number;
    icon: number;
    state: number;
}

export interface Achievement {
    AC_FIRST_ENTRY_SEEN: boolean;
    name: string;
    AC_FIRST_ENTRY_UNLOCKED_AT: number;
}

export interface WritingTemplate {
    id: number;
    order: number;
    title: string;
    body: string;
    predefined_template_id: number;
}

export interface Metadata {
    platform: string;
    number_of_entries: number;
    number_of_photos: number;
    created_at: number;
    ios_version: number;
    android_version: number;
}

export interface PreferredMoodIconsIdsForMoodIdsForIconsPack {
    [packId: string]: { [moodId: string]: number };
}

export interface DayEntry {
    assets: number[]; // id of the asset
    datetime: number;
    day: number;
    hour: number;
    minute: number;
    month: number; // 0 based index
    mood: number; // mood id
    note: string;
    note_title: string;
    tags: number[]; // id of the tag
    timeZoneOffset: number;
    year: number;
}

export interface DaylioBackupData {
    moodIconsDefaultFreePackId: number;
    goals: Goal[];
    goals_created_count: number;
    tag_groups: TagGroup[];
    customColorIdGood: number;
    isCustomColorPaletteActive: boolean;
    color_mode: string;
    goalSuccessWeeks: GoalSuccessWeek[];
    isReminderOn: boolean;
    isColorPaletteReversed: boolean;
    isBiometryAllowed: boolean;
    memoriesAllowedMoodGroupsIds: number[];
    prefs: Pref[];
    customColorIdAwful: number;
    customColorIdFugly: number;
    reminders: Reminder[];
    pinMode: number;
    version: number;
    isMemoriesNoteShownInNotification: boolean;
    dayEntries: DayEntry[]; // Not defined due to the complexity of the structure
    metadata: Metadata;
    daysInRowLongestChain: number;
    assets: Asset[];
    customColorIdGreat: number;
    goalEntries: GoalEntry[];
    platform: string;
    moodIconsPackId: number;
    customColorIdMeh: number;
    showNotificationAfterOneEntry: boolean;
    isMemoriesVisibleToUser: boolean;
    preferredMoodIconsIdsForMoodIdsForIconsPack: PreferredMoodIconsIdsForMoodIdsForIconsPack;
    pin: string;
    defaultColorPaletteId: number;
    autoBackupIsEnabled: boolean;
    isWeeklyNotificationsEnabled: boolean;
    milestones: Milestone[];
    colorPaletteId: number;
    customMoods: CustomMood[];
    isMemoriesReminderEnabled: boolean;
    healthKitConnectedDataTypes: any[]; // Not defined due to the complexity of the structure
    customColorPrimary: number;
    tags: Tag[];
    achievements: Achievement[];
    writingTemplates: WritingTemplate[];
}