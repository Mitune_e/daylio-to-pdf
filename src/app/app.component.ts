import { AfterViewInit, Component, OnInit, ViewChild, input } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { FileSelectEvent, UploadEvent } from 'primeng/fileupload';
import { DayEntry, DaylioBackupData, Tag } from '../interfaces/DaylioBackup';
import JSZip, { JSZipMetadata } from 'jszip';
import { environment } from '../environements/environement';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

export interface DateGroupedEntry {
    ISODate: string;
    prettyDate: string;
    entries: DayEntry[];
}

export interface AppDaylioMood {
  title: string;
  src: string;
  id:number;
}

export interface AppDalioTag {
  title: string;
  id:number;
  icon: number;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [ConfirmationService,MessageService]
})
export class AppComponent implements AfterViewInit {
  private moodColorMap: Record<string,string> = {
    amazing: '#991B81', 
    good: "#F23B7B",
    Meh: "#E97F02",
    bad: "#F7BA1E",
    irritated: "#F7BA1E",
    anxiety: "#F7BA1E",
    sad: "#F7BA1E",
    tired: "#F7BA1E",
    angry: "#A8BE1A",
    awdul: "#A8BE1A",
    distressed: "#A8BE1A",
    exhausted: "#A8BE1A",
    depressed: "#A8BE1A"
  }
  getMoodColor(index:number){
    const mood = this.daylioMoodsMapped[index]
    if(mood.title in this.moodColorMap) return this.moodColorMap[mood.title]
    else return this.moodColorMap['Meh']
  }

  constructor(private sanitizer: DomSanitizer, private messageService: MessageService, private confirmationService: ConfirmationService, ) { }
  @ViewChild('fileInput') fileInput: any;

  daylioData?: DaylioBackupData;
  entriesByDate: DateGroupedEntry[] = [];
  daylioAssets: {[key: string]: string} = {};
  daylioAssetsMapped: {[key: string]: string} = {};
  daylioLoadingProgess: string = '0%';
  daylioMoodsMapped: AppDaylioMood[] = [];
  daylioTagsMapped: AppDalioTag[] = [];
  showedEntriesCount = 10
  showHelp = true
  zip = new JSZip();

  ngAfterViewInit(){
    // if dev environment, load a default file
    if(environment.production === false){
      fetch('assets/example-export.daylio').then(async (response)=>{
        const blob = await response.blob();
        const file = new File([blob], 'backup.daylio');
        const event = {
          currentFiles: [file]
        } as FileSelectEvent
        this.onFileSelect(event)
      })
    }
  }

  fileProcessedCount = 0;
  fileCount = 0;

  initFileLoading(zip: JSZip){
    this.daylioLoadingProgess = '0%';
    this.fileCount = Object.keys(zip.files).length;
    this.fileProcessedCount = 0;
  }

  findDefaultMoodName(id: number){
    switch(id){
      case 1: return 'amazing';
      case 2: return 'good';
      case 3: return 'Meh';
      case 4: return 'bad';
      case 5: return 'awful';
      default: return 'Unknown';
    }
  }

  onFileSelect(event: FileSelectEvent) {
    this.zip.loadAsync(event.currentFiles[0]).then((zip)=>{
      this.initFileLoading(zip);
      let dataPromise: Promise<string>;
      let assetsPromises: Promise<string>[] = [];

      zip.forEach((relativePath, zipEntry)=>{
        if(relativePath === 'backup.daylio'){
          dataPromise = zipEntry.async('string')
        }
        else{ // everything else will be a picture
          const fileName = relativePath.split('/').pop() || 'error';
          // do not load real images in dev mode
          // if(environment.production === true){
          if(true){
          assetsPromises.push(new Promise((resolve, reject)=>{
            zipEntry.async('blob').then((blob)=>{
                const objectUrl = URL.createObjectURL(blob);
                this.daylioAssets[fileName] = objectUrl;
                this.fileProcessedCount++;
                this.daylioLoadingProgess = Math.round(this.fileProcessedCount / this.fileCount * 100) + '%';
                resolve(fileName);
              })
            }))
          }
          // place kitten in dev mode
          else{
            this.daylioAssets[fileName] = 'https://placekitten.com/g/300/300'
          }
        }
      })

      Promise.all(assetsPromises).then((fileNames)=>{
        return dataPromise
      }).then((content)=>{
        // parse daylio data
        const decoded = atob(content);
        const utf8Decoded = decodeURIComponent(escape(decoded));
        this.daylioData = JSON.parse(utf8Decoded) as DaylioBackupData;
        this.fileProcessedCount++;
        this.daylioLoadingProgess = Math.round(this.fileProcessedCount / this.fileCount * 100) + '%';
        
        // prepare images for easy access in template
        this.daylioData.assets.forEach((asset)=>{
          this.daylioAssetsMapped[asset.id] = this.daylioAssets[asset.checksum];
        })

        // prepare moods for easy access in template
        this.daylioData.customMoods.forEach((mood)=>{
          if(mood.custom_name == '') mood.custom_name = this.findDefaultMoodName(mood.id);
          this.daylioMoodsMapped[mood.id] = {
            title: mood.custom_name,
            src: '/assets/icons/mood-' + mood.id + '.png',
            id: mood.id
          }
        })

        // prepare tags for easy access in template
        this.daylioData.tags.forEach((tag)=>{
          this.daylioTagsMapped[tag.id] = {
            title: tag.name,
            id: tag.id,
            icon: tag.icon,
          }
        })

        // crypt notes so I cant read while dev
        // if(environment.production === false){
        //   this.daylioData.dayEntries.forEach((entry)=>{
        //     entry.note = btoa(entry.note);
        //     entry.note_title = btoa(entry.note_title);
        //   })
        // }

        // group entries by date for template

        // get all unique dates
        const dates: {prettyDate: string, ISODate: string}[] = []
        this.daylioData.dayEntries.forEach((entry)=>{
          // format like Monday, 4 December 2023
          const prettyDate = new Date(entry.year, entry.month, entry.day).toLocaleDateString('en-GB', {
            weekday: 'long',
            day: 'numeric',
            month: 'long',
            year: 'numeric'
          });

          // format like 2023-12-04
          const ISODate = new Date(entry.year, entry.month, entry.day).toISOString().split('T')[0];
          
          // check if we already have this date
          const filteredDates = dates.filter(d => d.ISODate === ISODate);
          if(filteredDates.length === 0){
            dates.push({
              prettyDate: prettyDate,
              ISODate: ISODate
            }); 
          }
        })

        // create a group for each date
        dates.forEach((date)=>{
          const entries = this.daylioData?.dayEntries
          .filter((entry)=>{
            const ISODate = new Date(entry.year, entry.month, entry.day).toISOString().split('T')[0];
            return ISODate === date.ISODate;
          }) || []

          this.entriesByDate.push({ISODate: date.ISODate, prettyDate: date.prettyDate, entries: entries});
        })

        // sort entries by year, month, day (DESC)
        this.entriesByDate.sort((a, b)=>{
          const dateA = new Date(a.ISODate);
          const dateB = new Date(b.ISODate);
          if(dateA > dateB) return -1;
          if(dateA < dateB) return 1;
          return 0;
        })

        // for each date, sort entries by time (ASC)
        this.entriesByDate.forEach((group)=>{
          group.entries.sort((a, b)=>{
            const dateA = new Date(a.year, a.month, a.day, a.hour, a.minute);
            const dateB = new Date(b.year, b.month, b.day, b.hour, b.minute);
            if(dateA > dateB) return 1;
            if(dateA < dateB) return -1;
            return 0;
          })
        })
        
      })
    })
  }

  onExport(){
    window.print()
  }

  onShowAll(){
    if(window.confirm("Careful, this may take a long time, and your browser might be unresponsive while the entries load. Also, it might crash if you don't have enough RAM Make sure you've saved any unsaved work before proceeding.")){
        this.showedEntriesCount = this.entriesByDate.length
    }
  }

  onHelpToggle(){
    this.showHelp = !this.showHelp
  }
}