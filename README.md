# About

This is a project showing a daylio preview given a daylio export file.
It is meant to export your daylio entries beautifully as a big pdf file, so you can print it (because Daylio native pdf export really sucks at time when this is written)


# About the .daylio file format

This is just a .zip file with a silly extension. it can be extracted.
This .zip archive contains a `backup.daylio` file, and an assets folder.


## Assets folder

The assets folder contains images saved in a date folder structure. like `yearFolder/monthFolder/imageChecksum`
image files don't have an extension, but they can be read as Jpeg


## backup.daylio file

the `backup.daylio` file contains a b64 encoded JSON string. When decoded, one can browse the whole daylio db of the exported file as structured data.
This project has a typescript interface for it : `src/app/interfaces/DaylioBackup.ts`


# Run for dev

`python -m http.server`
then go to [localhost:8000](http://localhost:8000)